import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {Storage} from "@ionic/storage";

@Injectable({
  providedIn: 'root'
})
export class SessionGuard implements CanActivate {

  constructor(
      private storage: Storage,
      private router: Router
  ) {
  }

  async canActivate() {
    const isSessionCorrect = await this.storage.get('isSessionCorrect')
    if(isSessionCorrect){
      return true
    }else{
      this.router.navigateByUrl('/login')
    }
  }
  
}
