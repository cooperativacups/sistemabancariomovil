export class Cliente {
     cedula: string;
     nombre: string;
     apellido: string;
     fechaNacimiento: string;
     direccion: string;
     telefono: string;
     operadora: string;
     correo: string;
     usuario: string;
     contrasena: string;

}
