import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreditoPageRoutingModule } from './credito-routing.module';

import { CreditoPage } from './credito.page';
import {NgCircleProgressModule} from "ng-circle-progress";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditoPageRoutingModule,
    NgCircleProgressModule.forRoot()
  ],
  declarations: [CreditoPage]
})
export class CreditoPageModule {}
