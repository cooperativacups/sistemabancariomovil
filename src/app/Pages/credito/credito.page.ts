import {Component, OnInit} from '@angular/core';
import {TransactionService} from "../../Services/transaction.service";
import {DatalocalService} from "../../Services/datalocal.service";
import {NavController} from "@ionic/angular";

@Component({
    selector: 'app-credito',
    templateUrl: './credito.page.html',
    styleUrls: ['./credito.page.scss'],
})
export class CreditoPage implements OnInit {

    selectedRange = 0;
    nombre: string
    cuenta: string
    saldo: string
    credito: any
    cuotasVencidas = new Array()
    session: string
    tablaAmor: any[]

    constructor(
        private transactionService: TransactionService,
        public dataLocalService: DatalocalService,
        private navController: NavController
    ) {
    }

    ngOnInit() {
        this.session = this.dataLocalService.data
        if (this.session != null) {
            console.log("SOY LA SESSION " + this.session)
            this.transactionService.creditBalanace(this.session).subscribe((creditoRecibido) => {
                this.credito = creditoRecibido
                this.nombre = creditoRecibido.cuentaAhorro.cliente.nombre + " " + creditoRecibido.cuentaAhorro.cliente.apellido
                this.cuenta = creditoRecibido.cuentaAhorro.numeroCuenta
                this.saldo = creditoRecibido.saldoCredito
                this.tablaAmor = creditoRecibido.tablaAmortizacion
                console.log(this.tablaAmor)
                for (let cuota of creditoRecibido.tablaAmortizacion) {
                    if (cuota.estadoCuota == 'VENCIDO') {
                        console.log(cuota)
                        this.cuotasVencidas.push(cuota)
                    }
                }
            }, (error) => {
                console.log(error)
            })
        } else {
            this.navController.navigateBack('/login')
        }
    }


}
