import {Component, OnInit} from '@angular/core';
import {LoadingController, ToastController} from "@ionic/angular";
import {Router} from "@angular/router";
import {Cliente} from "../../Models/cliente";
import {AuthenticateService} from "../../Services/authenticate.service";
import {Mensaje} from "../../Models/mensaje";
import {DatalocalService} from "../../Services/datalocal.service";
import {Storage} from "@ionic/storage";
import {tryCatch} from "rxjs/internal-compatibility";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    slideOpts = {
        allowSlidePrev: false,
        allowSlideNext: false
    };
    cliente: Cliente = new Cliente();
    mensajeRecibido: Mensaje
    sessionData: any

    loginForm: FormGroup;
    validation_messages = {
        email: [
            { type: "required", message: " El email es requerido" },
            { type: "pattern", message: "Este no es un email válido" }
        ],
        password: [
            { type: "required", message: " La contraseña es requerida" },
            { type: "minlength", message: "Minimo 5 letras para la contraseña" }
        ]
    };
    errorMessage: any;

    constructor(
        private authenticateService: AuthenticateService,
        public toastController: ToastController,
        public loadingController: LoadingController,
        private router: Router,
        public dataLocalService: DatalocalService,
        private storage: Storage,
        private formBuilder: FormBuilder
    ) {
        this.loginForm = this.formBuilder.group({
            email: new FormControl(
                "",
                Validators.compose([
                    Validators.required,
                    Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
                ])
            ),
            password: new FormControl(
                "",
                Validators.compose([Validators.required, Validators.minLength(5)])
            )
        });
    }

    ngOnInit() {
        this.loginForm.reset(this.loginForm.value)
    }

    async loginAction(credentials) {
        const loading = await this.loadingController.create({
            message: '',
            duration: 2000
        });
        await loading.present();
        try{
            this.cliente.usuario = credentials.email
            this.cliente.contrasena = credentials.password
            //console.log(this.cliente)
            this.storage.remove('isSessionCorrect')
            this.authenticateService.autenticar(this.cliente).subscribe((mensaje) => {
                    this.mensajeRecibido = mensaje
                    if (this.mensajeRecibido.mensaje == "OK") {
                        //console.log(this.mensajeRecibido)
                        //console.log("entre mijin")
                        loading.dismiss();
                        this.authenticateService.sessionVariables(this.cliente.usuario).subscribe((session)=>{
                            this.sessionData = session.numeroCuenta
                            this.storage.set('isSessionCorrect', this.sessionData)
                            //console.log(this.sessionData)
                            //console.log("obtuve la session")
                            if(this.sessionData != null){
                                this.dataLocalService.sessionCliente(this.sessionData)
                                this.mostrarMensaje('BIENVENIDO A CUPS MÓVIL');
                                this.router.navigate(['menu/saldo']);
                            }
                        },(error) =>{
                            this.mostrarMensaje('ERROR CREDENCIALES NO VALIDAS');
                            loading.dismiss();
                        })
                    }else{
                        if(this.mensajeRecibido.mensaje == "ERROR"){
                            //console.log(this.mensajeRecibido)
                            //this.errorMessage = "ERROR"
                            this.mostrarMensaje('USUARIO O PASSWORD INCORRECTOS');
                            loading.dismiss();
                        }
                    }
                },
                (error: HttpErrorResponse) => {
                    this.mostrarMensaje('VERIFIQUE SU CONEXION');
                    loading.dismiss();
                });
        }catch (error){
            this.mostrarMensaje('VERIFIQUE SU CONEXION');
            loading.dismiss();
        }
    }

    async mostrarMensaje(mensaje: string) {
        const toast = await this.toastController.create({
            message: mensaje,
            duration: 2000
        });
        toast.present();
    }

    changePassword(){
        this.router.navigate(['perfil/credenciales'])
    }

}
