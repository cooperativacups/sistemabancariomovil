import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'saldo', loadChildren: () => import('../saldo/saldo.module').then(m => m.SaldoPageModule)
      },
      {
        path: 'credito', loadChildren: () => import('../credito/credito.module').then(m => m.CreditoPageModule)
      },
      {
        path: 'transferencia', loadChildren: () => import('../transferencia/transferencia.module').then(m => m.TransferenciaPageModule)
      },
      {
        path: 'perfil', loadChildren: () => import('../perfil/perfil.module').then(m => m.PerfilPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
