import { Component, OnInit } from '@angular/core';
import {LoadingController, MenuController, NavController, ToastController} from "@ionic/angular";
import {DatalocalService} from "../../Services/datalocal.service";
import {Storage} from "@ionic/storage";
import {AuthenticateService} from "../../Services/authenticate.service";
import {TransactionService} from "../../Services/transaction.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  public session: string

  constructor(
      private menuController: MenuController,
      private navController: NavController,
      public dataLocalService: DatalocalService,
      private storage: Storage,
      private authenticateService: AuthenticateService,
      private  toastController: ToastController,
      private loadingController: LoadingController,
      private transaccionService: TransactionService
  ) { }

  ngOnInit() {

  }

  closeMenu() {
    this.menuController.close();
  }

  saldoPage(){
    this.verificarSession()
    this.navController.navigateRoot('/menu/saldo')
    this.menuController.close();
  }

  creditoPage(){
    this.verificarSession()
    this.navController.navigateRoot('/menu/credito')
    this.menuController.close();
  }

  transferenciaPage(){
    this.verificarSession()
    this.dataLocalService.clientes = []
    this.navController.navigateRoot('/menu/transferencia')
    this.menuController.close();
  }

  perfilPage(){
    this.verificarSession()
    this.navController.navigateRoot('/menu/perfil')
    this.menuController.close();
  }

  salir(){
    this.storage.remove('isSessionCorrect')
    this.dataLocalService.sessionDestroy()
    this.navController.navigateRoot('/login')
    this.menuController.close();
  }

  async verificarSession(){
    const loading = await this.loadingController.create({
      message: '',
      duration: 4000
    });
    this.session = this.dataLocalService.data
    if(this.session != null){
      this.transaccionService.accountBalanace(this.session).subscribe((session)=>{
        if(session==null){
          this.dataLocalService.sessionDestroy()
          this.mostrarMensaje('VERIFIQUE SU CONEXION');
          loading.dismiss();
          this.navController.navigateBack('/login')
        }
      },(error) =>{
        this.mostrarMensaje('VERIFIQUE SU CONEXION');
        loading.dismiss();
        this.navController.navigateBack('/login')
      })
    }
  }

  async mostrarMensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }


}
