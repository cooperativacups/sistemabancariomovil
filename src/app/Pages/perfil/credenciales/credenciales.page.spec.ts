import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CredencialesPage } from './credenciales.page';

describe('CredencialesPage', () => {
  let component: CredencialesPage;
  let fixture: ComponentFixture<CredencialesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredencialesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CredencialesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
