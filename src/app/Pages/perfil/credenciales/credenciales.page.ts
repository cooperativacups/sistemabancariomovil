import { Component, OnInit } from '@angular/core';
import {LoadingController, NavController, ToastController} from "@ionic/angular";
import {PasswordService} from "../../../../../../../clienteionic-master/src/app/Services/password.service";
import {Mensaje} from "../../../../../../../clienteionic-master/src/app/Models/mensaje";
import {Cliente} from "../../../../../../../clienteionic-master/src/app/Models/cliente";

@Component({
  selector: 'app-credenciales',
  templateUrl: './credenciales.page.html',
  styleUrls: ['./credenciales.page.scss'],
})
export class CredencialesPage implements OnInit {

  cliente: Cliente = new Cliente();
  mensajeRecibido: Mensaje
  constructor(
      private navController: NavController,
      private toastController: ToastController,
      private loadingController: LoadingController,
      private passwordService: PasswordService
  ) { }

  ngOnInit(
  ) {
  }

  cerrarCredenciales(){
    this.navController.navigateBack('login')
  }

  async changePassword(){
    const loading = await this.loadingController.create({
      message:'',
      duration: 2000
    })
    await loading.present()
    console.log(this.cliente.correo)
    this.passwordService.changePassword(this.cliente.correo).subscribe((mensaje)=>{
      this.mensajeRecibido = mensaje
          this.mensajeRecibido = mensaje
          if (this.mensajeRecibido.mensaje == "OK") {
            console.log(this.mensajeRecibido)
            console.log("entre mijin")
            loading.dismiss();
            this.mostrarMensaje('CAMBIO DE CONTRASEÑA EXITOSO');
            //this.router.navigate(['menu/saldo']);
          }else{
            if(this.mensajeRecibido.mensaje == "ERROR"){
              console.log(this.mensajeRecibido)
              this.mostrarMensaje('NO SE ENVIO SU SOLCICITUD DE CAMBIO');
              loading.dismiss();
            }
          }
    },
        error => {
          //console.log(error)
          //console.log(this.mensajeRecibido)
          this.mostrarMensaje('NO SE ENVIO SU SOLCICITUD DE CAMBIO');
          loading.dismiss();
        })
  }

  async mostrarMensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

}
