import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SaldoPageRoutingModule } from './saldo-routing.module';

import { SaldoPage } from './saldo.page';
import {RouterModule} from "@angular/router";
import {NgCircleProgressModule} from "ng-circle-progress";
import {RoundProgressModule} from 'angular-svg-round-progressbar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoundProgressModule,
    RouterModule.forChild([
      {
        path:'',
        component: SaldoPage
      }
    ]),
    NgCircleProgressModule.forRoot()
  ],
  declarations: [SaldoPage]
})
export class SaldoPageModule {}
