import {Component, OnInit} from '@angular/core';
import {TransactionService} from "../../Services/transaction.service";
import {DatalocalService} from "../../Services/datalocal.service";
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-saldo',
  templateUrl: './saldo.page.html',
  styleUrls: ['./saldo.page.scss'],
})
export class SaldoPage implements OnInit {

  public selectedRange = 0;
  public nombre: string
  public cuenta: string
  public saldo: string
  public cuentaAhorro: any
  public session: string

  max = 100
  current = 35

  constructor(
      private transaccionService: TransactionService,
      public dataLocalService: DatalocalService,
      private navController: NavController
  ) {
  }

  ngOnInit() {
    this.session = this.dataLocalService.data
    if (this.session != null) {
      console.log("SOY LA SESSION " + this.session)
      this.transaccionService.accountBalanace(this.session).subscribe(cuentaAhorro => {
        this.cuentaAhorro = cuentaAhorro
        this.nombre = cuentaAhorro.cliente.nombre + " " + cuentaAhorro.cliente.apellido
        this.cuenta = cuentaAhorro.numeroCuenta
        this.saldo = cuentaAhorro.saldo
        console.log(this.nombre)
        console.log(this.cuentaAhorro)
      }, (error => {
        console.log(error)
      }))
    } else {
      this.navController.navigateBack('/login')
    }
  }


}
