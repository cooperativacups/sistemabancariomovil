import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TipobeneficiariosPage } from './tipobeneficiarios.page';

const routes: Routes = [
  {
    path: '',
    component: TipobeneficiariosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TipobeneficiariosPageRoutingModule {}
