import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TipobeneficiariosPageRoutingModule } from './tipobeneficiarios-routing.module';

import { TipobeneficiariosPage } from './tipobeneficiarios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TipobeneficiariosPageRoutingModule
  ],
  declarations: [TipobeneficiariosPage]
})
export class TipobeneficiariosPageModule {}
