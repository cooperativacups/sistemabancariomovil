import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TipobeneficiariosPage } from './tipobeneficiarios.page';

describe('TipobeneficiariosPage', () => {
  let component: TipobeneficiariosPage;
  let fixture: ComponentFixture<TipobeneficiariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipobeneficiariosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TipobeneficiariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
