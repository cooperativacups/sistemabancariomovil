import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NavController} from "@ionic/angular";
import {Cliente} from "../../../Models/cliente";
import {TransactionService} from "../../../Services/transaction.service";
import {DatalocalService} from "../../../Services/datalocal.service";


@Component({
  selector: 'app-tipobeneficiarios',
  templateUrl: './tipobeneficiarios.page.html',
  styleUrls: ['./tipobeneficiarios.page.scss'],
})
export class TipobeneficiariosPage implements OnInit {

  segment: string = "beneficiario";
  filtro: string
  listadoClientesFiltro: any[] = []

  constructor(
      private router: Router,
      private navController: NavController,
      private transactionService: TransactionService,
      public dataLocalService: DatalocalService
  ) { }

  ngOnInit() {
  }

  loadBeneficiarioInterno(){
    //this.router.navigate(['menu/transferencia/clientesinternos'])
    this.navController.navigateForward('menu/transferencia/clientesinternos')
  }

  loadBeneficiarioExterno(){
    //this.router.navigate(['menu/transferencia/clientesexternos'])
    this.navController.navigateForward('menu/transferencia/clientesexternos')
  }

  cerrarTipoBeneficiario(){
    this.router.navigate(['menu/transferencia'])
    this.navController.navigateBack('menu/transferencia')
    this.dataLocalService.limpiarCliente()
  }

  async segmentChanged(event) {
    this.listadoClientesFiltro = []
    this.segment
  }

  buscarCliente(event){
    this.filtro = event.detail.value
    if(this.filtro.length > 0){
      this.transactionService.getClientesInternos(this.filtro).subscribe((listadoFiltro)=>{
        this.listadoClientesFiltro = listadoFiltro
      },(error)=>{
        console.log(error)
      })
    }else{
      this.listadoClientesFiltro = []
    }
  }

  cargarCliente(cliente){
    console.log(cliente)
    this.dataLocalService.guardarCliente(cliente)
    this.navController.navigateBack('menu/transferencia')
  }


}
