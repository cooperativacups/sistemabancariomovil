import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferenciaPage } from './transferencia.page';

const routes: Routes = [
  {
    path: '',
    component: TransferenciaPage
  },
  {
    path: 'tipobeneficiarios',
    loadChildren: () => import('./tipobeneficiarios/tipobeneficiarios.module').then( m => m.TipobeneficiariosPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransferenciaPageRoutingModule {}
