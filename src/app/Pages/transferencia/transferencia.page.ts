import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Cliente} from "../../Models/cliente";
import {DatalocalService} from "../../Services/datalocal.service";
import {Transferencia} from "../../Models/transferencia";
import {LoadingController, NavController, ToastController} from "@ionic/angular";
import {TransactionService} from "../../Services/transaction.service";
import {Mensaje} from "../../Models/mensaje";

@Component({
    selector: 'app-transferencia',
    templateUrl: './transferencia.page.html',
    styleUrls: ['./transferencia.page.scss'],
})
export class TransferenciaPage implements OnInit {

    cliente: any[]
    transferencia: Transferencia = new Transferencia()
    mensajeRecibido: Mensaje
    public session: string

    constructor(
        private router: Router,
        public dataLocalService: DatalocalService,
        private toastController: ToastController,
        private loadingController: LoadingController,
        private transactionService: TransactionService,
        private navController: NavController,
    ) {
    }

    ngOnInit() {
        this.session = this.dataLocalService.data
        if (this.session != null) {
            this.cliente = this.dataLocalService.clientes
            console.log('estoy en la transferencia')
            console.log(this.cliente)
        } else {
            this.navController.navigateBack('/login')
        }
    }

    loadBeneficiario() {
        this.router.navigate(['menu/transferencia/tipobeneficiarios'])
    }

    async saveTransferencia() {
        const loading = await this.loadingController.create({
            message: 'Guardando'
            //duration: 2000
        });
        await loading.present();
        try {
            this.session = this.dataLocalService.data
            console.log("SOY LA SESSION " + this.session)
            this.transferencia.cuentaOrigen = this.session
            for (let cli of this.cliente) {
                this.transferencia.cuentaDestino = cli.numeroCuenta
            }
            console.log(this.transferencia)
            this.transactionService.saveTransferenciaInterna(this.transferencia).subscribe((mensaje) => {
                    this.mensajeRecibido = mensaje
                    if (this.mensajeRecibido.mensaje == "TRANSFERENCIA REALIZADA CON ÉXITO") {
                        console.log(this.mensajeRecibido)
                        console.log("entre mijin")
                        //this.dataLocalService.clientes = []
                        loading.dismiss();
                        this.mostrarMensaje('TRANSFERENCIA EXITOSA');
                        this.dataLocalService.clientes = []
                        this.router.navigate(['menu/transferencia']);
                    } else {
                        if (this.mensajeRecibido.mensaje == "NO SE PUDO REALIZAR SU TRANFERECNIA") {
                            //this.dataLocalService.clientes = []
                            console.log(this.mensajeRecibido)
                            this.mostrarMensaje('TRANSFERENCIA ERRADA');
                            loading.dismiss();
                        } else {
                            this.mostrarMensaje(this.mensajeRecibido.mensaje);
                            loading.dismiss();
                        }
                    }
                },
                error => {
                    this.mostrarMensaje('VERIFIQUE SU CONEXION');
                    loading.dismiss();
                    this.navController.navigateBack('/login')
                });
        } catch (error) {
            this.mostrarMensaje('VERIFIQUE SU CONEXION');
            loading.dismiss();
            this.navController.navigateBack('/login')
        }

    }

    cancelarTransferencia() {
        this.dataLocalService.clientes = []
        this.navController.navigateForward('menu/transferencia')
    }

    async mostrarMensaje(mensaje: string) {
        const toast = await this.toastController.create({
            message: mensaje,
            duration: 4000
        });
        toast.present();
    }

}
