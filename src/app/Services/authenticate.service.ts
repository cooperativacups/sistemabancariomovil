import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Cliente} from "../Models/cliente";
import {Observable} from "rxjs";
import {Mensaje} from "../Models/mensaje";

const url = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor(
      public httpClient: HttpClient
  ) { }

  autenticar(cliente: Cliente): Observable<Mensaje> {
    return this.httpClient.post<Mensaje>(url + '/movil/authenticate', cliente);
  }

  sessionVariables(usuario: string): Observable<any> {
    return this.httpClient.get<any>(url + '/movil/session/'+usuario);
  }


}
