import {Injectable} from '@angular/core';
import {Cliente} from "../Models/cliente";
import {Storage} from "@ionic/storage";

@Injectable({
    providedIn: 'root'
})
export class DatalocalService {

    clientes: any[] = []
    data: string

    constructor(
        private storage: Storage
    ) {
    }

    sessionCliente(data: string) {
        this.data = data
    }

    sessionDestroy() {
        this.data = ""
    }

    guardarCliente(cliente: any) {
        const existeCliente = this.clientes.find(cli => cli.cliente.cedula === cliente.cliente.cedula)
        if (!existeCliente) {
            this.clientes.push(cliente)
        }
    }

    limpiarCliente() {
        this.storage.remove('cliente')
        this.clientes = []
    }

}
