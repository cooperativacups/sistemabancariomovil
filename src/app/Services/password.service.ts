import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Cliente} from "../../../../../clienteionic-master/src/app/Models/cliente";
import {Observable} from "rxjs";
import {Mensaje} from "../../../../../clienteionic-master/src/app/Models/mensaje";
import {environment} from "../../../../../clienteionic-master/src/environments/environment";

const url = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor(
      public httpClient: HttpClient
  ) { }

  changePassword(correo: string): Observable<Mensaje> {
    return this.httpClient.get<Mensaje>(url + '/movil/password/'+ correo);
  }
}
