import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment.prod";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Mensaje} from "../Models/mensaje";
import {Transferencia} from "../Models/transferencia";

const url = environment.apiUrl;

@Injectable({
    providedIn: 'root'
})
export class TransactionService {

    constructor(
        public httpClient: HttpClient
    ) {
    }

    accountBalanace(cuentaAhorro: string): Observable<any> {
        return this.httpClient.get<any>(url + '/movil/resumenCuenta/' + cuentaAhorro);
    }

    creditBalanace(credito: string): Observable<any> {
        return this.httpClient.get<any>(url + '/movil/resumenCredito/' + credito);
    }

    getInstituciones(): Observable<any[]> {
        return this.httpClient.get<any[]>(url + '/movil/institucionesFinancieras');
    }

    getClientesInternos(cuenta: string): Observable<any[]> {
        return this.httpClient.get<any[]>(url + '/movil/clientesInternos/' + cuenta);
    }

    getClientesExternos(cedula: string): Observable<any[]> {
        return this.httpClient.get<any[]>(url + '/movil/clientesExternos/' + cedula);
    }

    saveTransferenciaInterna(transferencia: Transferencia): Observable<Mensaje> {
        return this.httpClient.post<Mensaje>(url + '/movil/transferenciaInterna/', transferencia);
    }

    saveTransferenciaExterna(transferencia: Transferencia): Observable<Mensaje> {
        return this.httpClient.post<Mensaje>(url + '/movil/transferenciaExterna/', transferencia);
    }

}
